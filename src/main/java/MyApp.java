import java.util.Map;
import java.util.TreeMap;

public class MyApp {

	public static void main(String[] args) {
		if (args.length == 0) {
			throw (new IllegalArgumentException("Please run program again with input argument"));
		}
		Map<Character, Integer> letterCounter = new TreeMap<Character, Integer>();
		
		for (String word : args) {
			for (Character letter : word.toLowerCase().toCharArray()) {
				if (Character.isAlphabetic(letter)) {
					if (!letterCounter.containsKey(letter)) {
						letterCounter.put(letter, 0);
					}
					letterCounter.put(letter, letterCounter.get(letter) + 1);
				}
			}
		}

		System.out.print("Input: ");
		for (String word : args) {
			System.out.print(" "+word);
		}
		System.out.println("\nCounted Letters:");
		for (Character letter : letterCounter.keySet()) {
			System.out.println(letter + " = " + letterCounter.get(letter));
		}
	}

}
